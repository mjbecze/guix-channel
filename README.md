# Synopsis 

This is my personal Guix channel.

# Installation

[Install](https://www.gnu.org/software/guix/download/) Guix.

My packages can be added as a [Guix channel](https://www.gnu.org/software/guix/manual/en/html_node/Channels.html).  To do so, add it to
`~/.config/guix/channels.scm`:

```scheme
  (cons* (channel
          (name 'nullradix)
          (url "https://gitlab.com/mjbecze/guix-channel.git"))
         %default-channels)
```
Or if you have this repo checked out locally you can do the following

```scheme
(channel
     (name 'nullradix)
     (url "/path/to/repo"))
```
Then run `guix pull`.

