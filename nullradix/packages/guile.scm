(define-module (nullradix packages guile)
  #:use-module (guix git-download)
  #:use-module (guix build-system guile)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages guile-xyz)
  #:use-module ((gnu packages guile)))

(define-public guile3.0-syntax-highlight-javascript
  (package
    (name "guile3.0-syntax-highlight-javascript")
    (version "0.0.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
		    (url "https://gitlab.com/mjbecze/guile-syntax-highlight-javascript.git")
		    (commit "bd7c2bc7a80d8a2d42056f79587954c96e61c3d9")))
              (sha256
               (base32
		"04jxaa2ngxipy56pwnwiby5hsm9i6n5dazhqff3iq1ii40fp4pdz"))
              (file-name (git-file-name name version))))
    (build-system guile-build-system)
    (propagated-inputs
     `(("guile-syntax-highlight" ,guile3.0-syntax-highlight)))
    (inputs
     `(("guile" ,guile-3.0)))
    (home-page "https://gitlab.com/mjbecze/guile-syntax-highlight-javascript")
    (synopsis "Javscript highlighting for guile-syntax-highlight")
    (description "Javscript highlighting for guile-syntax-highlight")
    (license license:gpl3+)))
